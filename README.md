[![](https://images.microbadger.com/badges/version/kchilly/php-fpm.svg)](https://microbadger.com/images/kchilly/php-fpm "Get your own version badge on microbadger.com")
[![](https://images.microbadger.com/badges/image/kchilly/php-fpm.svg)](https://microbadger.com/images/kchilly/php-fpm "Get your own image badge on microbadger.com")  
[![dockeri.co](https://dockeri.co/image/kchilly/php-fpm)](https://hub.docker.com/r/kchilly/php-fpm)

# docker-php-fpm
This repository is the source of the `kchilly/php-fpm` [docker image](https://hub.docker.com/r/kchilly/php-fpm)

## Base
This image is based on `php:7.4-fpm` [official docker image](https://hub.docker.com/_/php).
