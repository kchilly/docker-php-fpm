FROM php:7.4-fpm

# Application home path in the container
ENV APPLICATION_HOME   /var/www/site

# Setup system packages
RUN set -x \
    && apt-get update --quiet \
    && apt-get install --quiet --yes --no-install-recommends \
        git \
        curl \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libpq-dev \
        gnupg1 \
        gnupg2 \
        libmcrypt-dev \
        zlib1g-dev \
        libxml2-dev \
        libzip-dev \
        libonig-dev \
        graphviz \
        libonig-dev

# Setup Php extensions
RUN docker-php-ext-install -j$(nproc) gd mbstring zip sockets pcntl \
    && pecl install redis apcu \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd \
    && docker-php-ext-install pdo pdo_pgsql pgsql pdo_mysql mysqli \
    && docker-php-ext-enable redis \
    && docker-php-ext-enable apcu

# Get Composer
RUN curl -ksS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer

RUN apt-get clean \
    && mkdir -p "${APPLICATION_HOME}"
